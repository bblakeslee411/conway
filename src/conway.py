import argparse
import curses
import numpy as np
import scipy.ndimage as sni
import signal
import sys
import time

interruptFlag = False


def initGrid(rows, cols, threshold):
    return np.random.choice((0, 1), size=(rows, cols),
                            p=(1 - threshold, threshold))


def updateGrid(oldBoard):
    newBoard = np.zeros(oldBoard.shape).astype(np.int)
    neighborFilter = np.array([[1, 1, 1],
                               [1, 0, 1],
                               [1, 1, 1]])
    neighborCounts = sni.convolve(oldBoard, neighborFilter, mode='wrap')
    survive = (oldBoard == 1) & ((neighborCounts == 2) | (neighborCounts == 3))
    newCell = (oldBoard == 0) & (neighborCounts == 3)
    newBoard[survive | newCell] = 1
    return newBoard


def interruptHandler(signal, frame):
    global interruptFlag
    interruptFlag = not interruptFlag


def main(screen):
    parser = argparse.ArgumentParser()
    parser.add_argument('--initThreshold', type=float, required=False,
                        default=0.33,
                        help='Probability of spawning live cell.')
    parser.add_argument('--rows', type=int, required=False, default=10,
                        help='Number of rows.')
    parser.add_argument('--cols', type=int, required=False, default=10,
                        help='Number of columns.')
    parser.add_argument('--delay', type=float, required=False, default=0.5,
                        help='Delay between frame updates in seconds.')
    args = parser.parse_args()

    signal.signal(signal.SIGINT, interruptHandler)
    curses.start_color()
    curses.use_default_colors()
    screen.addstr(0, 0, 'Conway\'s Game of Life')
    screen.addstr(1, 0, 'Press Ctrl+C to exit')

    board = initGrid(args.rows, args.cols, args.initThreshold)
    while True:
        screen.addstr(2, 0, ' ' + str(board)[1:-1] + '\n')
        screen.refresh()
        board = updateGrid(board)
        time.sleep(args.delay)
        if interruptFlag:
            sys.exit()


if __name__ == '__main__':
    curses.wrapper(main)
