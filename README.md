# John Conway's Game of Life

A small implementation of the Game of Life.

## Installation

Dependencies can be installed with:

```pip3 install -r requirements.txt```

## Invocation

A default 10x10 simulation can be started with:

```python3 conway.py```

Supported command line options:

```--initThreshold```:  Probability of spawning a live cell during initialization.  Must be between 0 and 1.

```--rows```:  Number of rows in the grid.

```--cols```:  Number of columns in the grid.

```--delay```:  Number of seconds to wait between updates.

Sample invocation with a 50% live spawn rate, 15x15 grid, and a 1 second update rate:

```python3 conway.py --initThreshold 0.5 --rows 15 --cols 15 --delay 1```